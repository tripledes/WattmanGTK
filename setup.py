# This file is part of WattmanGTK.
#
# Copyright (c) 2018 Bouke Haarsma
#
# WattmanGTK is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 2 of the License.
#
#
# WattmanGTK is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with WattmanGTK.  If not, see <http://www.gnu.org/licenses/>.
from setuptools import setup

setup(
    name='WattmanGTK',
    version='0.0.1',
    description='A Wattman-like GTK3+ GUI for AMDGPU users',
    long_description=(
        'This is a Python3 program which uses a simple GTK gui to view, monitor and in the'
        'future overclock a Radeon GPU on Linux.'
    ),
    url='https://gitlab.com/tripledes/WattmanGTK',
    author='Sergi Jimenez',
    author_email="tripledes@gmail.com",
    classifiers=[
        'Development Status :: Development Status :: 1 - Planning',
        'Environment :: Console :: Curses',
        'Environment :: X11 Applications :: GTK',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Desktop Environment',
        'Topic :: System :: Hardware',
        'Topic :: Utilities',
    ],
    license="GPL",
    keywords='amdgpu, tuning, monitoring',
    package_dir={"WattmanGTK":  "WattmanGTK"},
    packages=["WattmanGTK"],
    python_requires='>=3.6, <4',
    install_requires=[
        'pygobject',
        'matplotlib',
        'pycairo',
        'urwid>=2.1.2'
    ],
    extras_require={
        'dev': ['pycodestyle', 'pytest', 'coverage'],
        'test': ['pytest', 'coverage'],
    },
    package_data={
        "WattmanGTK": [
            "data/wattman.ui",
            "data/WattmanGTK.eps",
            "data/WattmanGTK.svg",
            "data/WattmanGTK-outline-type.svg",
        ]
    },
    entry_points={
        "console_scripts": ["wattmanGTK=WattmanGTK.wattman:main"]
    },
    project_urls={
        "Source": "https://gitlab.com/tripledes/WattmanGTK",
        "Tracker": "https://gitlab.com/tripledes/WattmanGTK/-/issues",
    }
)
